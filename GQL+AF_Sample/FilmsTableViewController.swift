//
//  FilmsTableViewController.swift
//  GQL+AF_Sample
//
//  Created by Felix Nievelstein on 29.09.16.
//  Copyright © 2016 Felix Nievelstein. All rights reserved.
//

import UIKit
import Alamofire
import GraphQLicious

class FilmsTableViewController: UITableViewController {

    var films = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadData()
    }
    
    func loadData()
    {
        let filmContent = Fragment(
            withAlias: "filmContent",
            name: "Film",
            fields: ["title", "id", "episodeID"]
        )
        let filmContentRequest = Request(withAlias: "test2", name: "films", arguments: [], fields: [filmContent])
        
        let filmsContent = Fragment(
            withAlias: "filmsContent",
            name: "FilmsConnection",
            fields: ["totalCount", filmContentRequest]
        )
        
        let myQuery = Query(request: Request(withAlias: "test", name: "allFilms", arguments: [], fields: [filmsContent]), fragments:[filmsContent, filmContent])
        
        print(myQuery.create())
        
        let manager = SessionManager.default
        
        // Specifying the Headers we need
        manager.session.configuration.httpAdditionalHeaders = [
            "Content-Type": "application/graphql",
            "Accept": "application/json" //Optional
        ]
        
        let string = myQuery.create().addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        
        Alamofire.request("https://graphql-swapi.parseapp.com/?query=" + string!).responseJSON{ response in
            
            if let result = response.result.value
            {
                let JSON = result as! NSDictionary
                let data = JSON["data"] as! NSDictionary
                let test = data["test"] as! NSDictionary
                self.films = test["test2"] as! NSArray
                
                DispatchQueue.main.async {
                    self.tableView .reloadData()
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return films.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "filmCell", for: indexPath)

        if let film = films.object(at: indexPath.row) as? NSDictionary
        {
            cell.textLabel?.text = film["title"] as? String
            if let episode = film["episodeID"]
            {
                cell.detailTextLabel?.text = "Episode \(episode)"
            }
            
        }

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "filmDetailSegue"
        {
            let filmDVC = segue.destination as! FilmDetailViewController
            let indexPath = self.tableView.indexPathForSelectedRow
            
            if let film = films.object(at: (indexPath?.row)!) as? NSDictionary
            {
                filmDVC.filmID = film["id"] as? String
            }
        }
    }
    

}

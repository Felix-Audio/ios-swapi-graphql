//
//  SWAPIGQLHandler.swift
//  GQL+AF_Sample
//
//  Created by Felix Nievelstein on 30.09.16.
//  Copyright © 2016 Felix Nievelstein. All rights reserved.
//

import UIKit
import Alamofire
import GraphQLicious

protocol SWAPIGALHandlerDelegate
{
    func receivedGQLResults(gqlData: NSDictionary)
}

class SWAPIGQLHandler: NSObject {

    var delegate: SWAPIGALHandlerDelegate?
    
    func loadFilm(id: String)
    {
        let myQuery = Query(request: Request(withAlias: "loadFilm", name: "film", arguments: [Argument(key: "id", value: id)], fields: ["title", "director", "openingCrawl", "releaseDate", "episodeID"]), fragments:[])
        print(myQuery.create())
        loadGQLData(query: myQuery)
    }
    
    func loadGQLData(query: Query)
    {
        let manager = SessionManager.default
        
        // Specifying the Headers we need
        manager.session.configuration.httpAdditionalHeaders = [
            "Content-Type": "application/graphql",
            "Accept": "application/json" //Optional
        ]
        
        let string = query.create().addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        
        Alamofire.request("https://graphql-swapi.parseapp.com/?query=" + string!).responseJSON{ response in
            
            if let result = response.result.value
            {
                let JSON = result as! NSDictionary
                print(JSON)
                
                if let error = JSON.object(forKey: "errors") as? NSArray
                {
                    self.handleGQLError(error: error)
                }
                else
                {
                    if let data = JSON["data"] as? NSDictionary
                    {
                        DispatchQueue.main.async {
                            if self.delegate != nil
                            {
                                self.delegate?.receivedGQLResults(gqlData: data)
                            }
                        }
                        
                    }
                }
            }
        }
    }
    
    func handleGQLError(error: NSArray)
    {
        print(error)
    }

}

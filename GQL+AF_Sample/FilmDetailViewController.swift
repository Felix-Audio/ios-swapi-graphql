//
//  FilmDetailViewController.swift
//  GQL+AF_Sample
//
//  Created by Felix Nievelstein on 30.09.16.
//  Copyright © 2016 Felix Nievelstein. All rights reserved.
//

import UIKit

class FilmDetailViewController: UIViewController {

    var filmID: String?
    @IBOutlet var directorLabel: UILabel!
    @IBOutlet var episodeLabel: UILabel!
    @IBOutlet var releaseDateLabel: UILabel!
    let swapiHandler = SWAPIGQLHandler()
    
    @IBOutlet var crawlTextView: UITextView!
    
    @IBOutlet var loadIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(filmID)
        
        if let id = filmID
        {
            loadIndicator.startAnimating()
            swapiHandler.delegate = self
            swapiHandler.loadFilm(id: id)
        }
        else
        {
            handleFilmError()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleFilmError()
    {
        loadIndicator.stopAnimating()
        self.navigationItem.title = "Error no film"
    }

}

extension FilmDetailViewController: SWAPIGALHandlerDelegate
{
    func receivedGQLResults(gqlData: NSDictionary)
    {
        if let film = gqlData["loadFilm"] as? NSDictionary
        {
            self.navigationItem.title = film["title"] as? String
            self.directorLabel.text = film["director"] as? String
            self.crawlTextView.text = film["openingCrawl"] as? String
            if let episode = film["episodeID"]
            {
                self.episodeLabel.text = "Episode \(episode)"
            }
            if let releaseDate = film["releaseDate"]
            {
                self.releaseDateLabel.text = "\(releaseDate)"
            }
            loadIndicator.stopAnimating()
        }
        else
        {
            handleFilmError()
        }
    }
}

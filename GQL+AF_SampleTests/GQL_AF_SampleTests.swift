//
//  GQL_AF_SampleTests.swift
//  GQL+AF_SampleTests
//
//  Created by Felix Nievelstein on 29.09.16.
//  Copyright © 2016 Felix Nievelstein. All rights reserved.
//

import XCTest
@testable import GQL_AF_Sample

class GQL_AF_SampleTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
